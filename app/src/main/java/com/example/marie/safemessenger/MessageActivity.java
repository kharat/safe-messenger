package com.example.marie.safemessenger;

import android.app.Activity;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.util.ArrayList;
import java.util.List;


public class MessageActivity extends Activity implements View.OnClickListener, AsyncResponse {

    TextView        contactLabel;
    ListView        msgsListView;
    EditText        sendEdit;
    Button          sendBtn;
    String          userName, friendName;
    List<Message>   messages;
    MessagesAdapter msgAdapter;

    void setupAttributes(){
        friendName = getIntent().getExtras().getString("friendname");
        userName = getIntent().getExtras().getString("account");
        contactLabel = (TextView) findViewById(R.id.contact_name_label);
        msgsListView = (ListView) findViewById(R.id.messages_listview);
        sendEdit = (EditText) findViewById(R.id.send_edit);
        sendBtn = (Button) findViewById(R.id.send_btn);
        sendBtn.setOnClickListener(this);
        contactLabel.setText(friendName);
        launchGetMessagesRequest(friendName);
    }

    void    launchGetMessagesRequest(String name) {
        HttpGet         getRequest = new HttpGet("http://vps120211.ovh.net:8080/safemessenger/rest/messages/get?from=" + friendName);
        String          token = userName + ":" + getIntent().getExtras().getString("password");

        getRequest.addHeader("Authorization", "Basic " + Base64.encodeToString(token.getBytes(), Base64.NO_WRAP));
        new RequestMaker(this, this, getRequest).execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        setupAttributes();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getResponse(String response) {
        JsonElement     parsed = new JsonParser().parse(response);
        JsonObject      json = parsed.getAsJsonObject();

        if (json.get("request").getAsString().equals("get messages"))  {
            if (json.get("status").getAsString().equals("OK")) {
                JsonArray       messagesArray = json.get("messages").getAsJsonArray();

                messages = new ArrayList<Message>();
                for (int i = 0; i < messagesArray.size(); i++) {
                    Gson        gson = new Gson();
                    Message     currentMessage;

                    currentMessage = gson.fromJson(messagesArray.get(i).getAsJsonObject(), Message.class);
                    currentMessage.setTimestamp(messagesArray.get(i).getAsJsonObject().get("timestamp").getAsLong());
                    messages.add(currentMessage);
                }
                msgAdapter = new MessagesAdapter(this, R.layout.message, messages, userName);
                msgsListView.setAdapter(msgAdapter);
            }
            else
                Toast.makeText(this, json.get("error").getAsString(), Toast.LENGTH_SHORT).show();
        }
        if (json.get("request").getAsString().equals("send message")) {
            if (json.get("status").getAsString().equals("OK")) {
                Toast.makeText(this, "Message sent", Toast.LENGTH_SHORT).show();
                launchGetMessagesRequest(friendName);
            }
            else
                Toast.makeText(this, json.get("error").getAsString(), Toast.LENGTH_SHORT).show();
        }
        if (json.get("request").getAsString().equals("remove message")) {
            if (json.get("status").getAsString().equals("OK")) {
                Toast.makeText(this, "Message removed", Toast.LENGTH_SHORT).show();
            }
            else
                Toast.makeText(this, json.get("error").getAsString(), Toast.LENGTH_SHORT).show();
            launchGetMessagesRequest(friendName);
        }
    }

    public void    removeMessage(View v) {
        RelativeLayout      parent = (RelativeLayout) v.getParent();
        TextView            idtv = (TextView) parent.getChildAt(4);
        String              id = idtv.getText().toString();

        HttpPost postRequest = new HttpPost("http://vps120211.ovh.net:8080/safemessenger/rest/messages/remove");
        String          token = getIntent().getExtras().getString("account") + ":" + getIntent().getExtras().getString("password");
        List            nameValuePairs = new ArrayList();

        nameValuePairs.add(new BasicNameValuePair("_id", id));
        try {
            postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("debug", "Exception raised during post.setEntity()");
        }
        postRequest.addHeader("Authorization", "Basic " + Base64.encodeToString(token.getBytes(), Base64.NO_WRAP));
        postRequest.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=UTF-8");
        new RequestMaker(this, this, postRequest).execute();
    }

    void launchSendMessageRequest() {
        String      content = sendEdit.getText().toString();

        if (content.length() > 0) {
            HttpPost postRequest = new HttpPost("http://vps120211.ovh.net:8080/safemessenger/rest/messages/send");
            String          token = getIntent().getExtras().getString("account") + ":" + getIntent().getExtras().getString("password");
            List            nameValuePairs = new ArrayList();

            nameValuePairs.add(new BasicNameValuePair("receiver", friendName));
            nameValuePairs.add(new BasicNameValuePair("content", content));
            try {
                postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("debug", "Exception raised during post.setEntity()");
            }
            postRequest.addHeader("Authorization", "Basic " + Base64.encodeToString(token.getBytes(), Base64.NO_WRAP));
            postRequest.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=UTF-8");
            new RequestMaker(this, this, postRequest).execute();
            sendEdit.setText("");
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.send_btn) {
            launchSendMessageRequest();
        }
    }
}
