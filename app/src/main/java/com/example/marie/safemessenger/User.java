package com.example.marie.safemessenger;

/**
 * Created by marie on 2/11/15.
 */
public class User {
    String name;
    int number;

    User (String n) {
        name = n;
        number = 0;
    };
    void setName (String n) {
        name = n;
    };
    void setNumber (int n){
        number = n;
    };
    String getName() {
        return name;
    };
    int getNumber() {
        return number;
    };
}
