package com.example.marie.safemessenger;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.protocol.HTTP;


public class SignUp extends ActionBarActivity implements View.OnClickListener, AsyncResponse{
    EditText account, password, password2;
    TextView errorLabel;
    Button save;

    private void setUpAttributes() {
        account = (EditText) findViewById(R.id.accountedit);
        password = (EditText) findViewById(R.id.pwdedit);
        password2 = (EditText) findViewById(R.id.pwdedit2);
        save = (Button) findViewById(R.id.savebutton);
        errorLabel = (TextView) findViewById(R.id.error_label);
        save.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setUpAttributes();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.savebutton) {
            if (account.getEditableText().toString().length() < 4 || account.getEditableText().toString().length() > 15)
                errorLabel.setText("Account name must have between 4 and 15 characters");
            else if (password.getEditableText().toString().length() < 4 || password.getEditableText().toString().length() > 15)
                errorLabel.setText("Password must have between 4 and 15 characters");
            else if (!password.getEditableText().toString().equals(password2.getEditableText().toString()))
                errorLabel.setText("Passwords do not match");
            else
                launchSignupRequest();
        }
    }

    void    launchSignupRequest() {
        HttpPost        postRequest = new HttpPost("http://vps120211.ovh.net:8080/safemessenger/rest/users/signup");

        String      token = account.getEditableText().toString() + ":" + password.getEditableText().toString();
        postRequest.addHeader("Authorization", "Basic " + Base64.encodeToString(token.getBytes(), Base64.NO_WRAP));
        postRequest.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=UTF-8");
        new RequestMaker(this, this, postRequest).execute();
    }

    @Override
    public void getResponse(String response) {
        JsonElement     parsed = new JsonParser().parse(response);
        JsonObject      json = parsed.getAsJsonObject();

        if (json.get("status").getAsString().equals("OK")) {
            Intent      intent = new Intent(this, ContactActivity.class);

            intent.putExtra("Authorization", json.get("token").getAsString());
            intent.putExtra("account", account.getText().toString());
            intent.putExtra("password", password.getText().toString());
            startActivity(intent);
        }
        else {
            Toast.makeText(this, json.get("error").getAsString(), Toast.LENGTH_SHORT).show();
        }
    }
}
