package com.example.marie.safemessenger;

/**
 * Created by HK on 2/12/2015.
 */
public interface AsyncResponse {
    public void     getResponse(String response);
}
