package com.example.marie.safemessenger;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStreamReader;

/**
 * Created by marie on 2/12/15.
 */

public class RequestMaker extends AsyncTask<String, Integer, String> {
    Context         context;
    AsyncResponse   delegate;
    HttpRequestBase request;
    ProgressDialog  dialog;

    RequestMaker(Context c, AsyncResponse delegate, HttpRequestBase request){
        context = c;
        this.delegate = delegate;
        this.request = request;
    }

    @Override
    protected void  onPreExecute() {
        dialog = new ProgressDialog(context);
        dialog.setTitle("Please wait");
        dialog.show();
    }

    /* Bizarre. On peut get n'importe quel contenu web, mais de notre serveur rest, ça n'affiche que les erreurs ._. */
    @Override
    protected String doInBackground(String... url) {
        HttpClient          client = new DefaultHttpClient();
        HttpEntity          entity;
        HttpResponse        response;
        InputStreamReader   streamReader;
        StringBuilder       sb = new StringBuilder();
        String              result = "NONE";

        try {
            response = client.execute(request);
            entity = response.getEntity();
            Log.d("debug", "Response status " + response.getStatusLine().getStatusCode());
            streamReader = new InputStreamReader(entity.getContent());
            for (int b = 0; b != -1; b = streamReader.read())
                sb.append((char) b);
            result = sb.toString().substring(sb.toString().indexOf("{"), sb.toString().lastIndexOf("}") + 1);
            streamReader.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("debug", "Exception raised during request " + request.getURI());
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        if (dialog.isShowing())
            dialog.dismiss();
        delegate.getResponse(result);
    }
}

