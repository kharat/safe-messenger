package com.example.marie.safemessenger;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

/**
 * Created by marie on 2/8/15.
 */
public class FriendsAdapter extends ArrayAdapter<String> {
    List<String> users;
    int layoutId;

    FriendsAdapter(Context context, int resourceId, List<String> users) {
        super(context, resourceId, users);
        this.users = users;
        layoutId = resourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageButton button;
        int resourceId = R.id.contactNameText;

        Context context = getContext();
        View retView = (convertView == null ? View.inflate(context, layoutId, null) : convertView);
        ((TextView) retView.findViewById(resourceId)).setText(users.get(position));
        Typeface face = Typeface.createFromAsset(getContext().getAssets(),"Vonique.ttf");
        ((TextView) retView.findViewById(resourceId)).setTypeface(face);
        button = (ImageButton) retView.findViewById(R.id.deleteButton);
        //button.setOnClickListener((View.OnClickListener) context);
        return retView;
    }
}
