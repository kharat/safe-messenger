package com.example.marie.safemessenger;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;



public class ContactActivity extends Activity implements View.OnClickListener, AsyncResponse {
    EditText        contactName;
    Button          addContactButton;
    ListView        contactList;
    List<String>    users;
    FriendsAdapter  adapter;

    private void setUpAttribute() {
        contactName = (EditText) findViewById(R.id.contactNameEdit);
        addContactButton = (Button) findViewById(R.id.addContactButton);
        contactList = (ListView) findViewById(R.id.contactListView);
        addContactButton.setOnClickListener(this);
        users = new ArrayList<String>();
        InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(contactName.getWindowToken(), 0);
        launchGetFriendsRequest();
    }

    void        launchGetFriendsRequest() {
        HttpGet         getRequest = new HttpGet("http://vps120211.ovh.net:8080/safemessenger/rest/friends/get");
        String          token = getIntent().getExtras().getString("account") + ":" + getIntent().getExtras().getString("password");

        getRequest.addHeader("Authorization", "Basic " + Base64.encodeToString(token.getBytes(), Base64.NO_WRAP));
        new RequestMaker(this, this, getRequest).execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        setUpAttribute();
    }

    void        launchAddFriendRequest() {
        HttpPost    postRequest = new HttpPost("http://vps120211.ovh.net:8080/safemessenger/rest/friends/add");
        String          token = getIntent().getExtras().getString("account") + ":" + getIntent().getExtras().getString("password");
        List            nameValuePairs = new ArrayList();

        nameValuePairs.add(new BasicNameValuePair("friendname", contactName.getText().toString()));
        try {
            postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("debug", "Exception raised during post.setEntity()");
        }
        postRequest.addHeader("Authorization", "Basic " + Base64.encodeToString(token.getBytes(), Base64.NO_WRAP));
        postRequest.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=UTF-8");
        new RequestMaker(this, this, postRequest).execute();
    }

    public void     friendButtonClicked(View v) {
        LinearLayout vwParentRow = (LinearLayout)v.getParent();

        TextView contactTv = (TextView)vwParentRow.getChildAt(0);
        launchRemoveFriendRequest(contactTv.getText().toString());
    }

    void    launchRemoveFriendRequest(String name) {
        HttpPost        postRequest = new HttpPost("http://vps120211.ovh.net:8080/safemessenger/rest/friends/remove");
        String          token = getIntent().getExtras().getString("account") + ":" + getIntent().getExtras().getString("password");
        List            nameValuePairs = new ArrayList();

        nameValuePairs.add(new BasicNameValuePair("friendname", name));
        try {
            postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("debug", "Exception raised during post.setEntity()");
        }
        postRequest.addHeader("Authorization", "Basic " + Base64.encodeToString(token.getBytes(), Base64.NO_WRAP));
        postRequest.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=UTF-8");
        new RequestMaker(this, this, postRequest).execute();
        contactName.setText("");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.addContactButton) {
            if (contactName.getEditableText().toString().length() > 0) {
                launchAddFriendRequest();
            }
            else
                Toast.makeText(this, "You must specify a username", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void getResponse(String response) {
        JsonElement         parsed = new JsonParser().parse(response);
        JsonObject          json = parsed.getAsJsonObject();

        if (json.get("request").getAsString().equals("get friends")) {
            if (json.get("status").toString().contains("OK")) {
                JsonArray friendsArray = json.get("friends").getAsJsonArray();

                users = new ArrayList<String>();
                for (int i = 0; i < friendsArray.size(); i++) {
                    users.add(friendsArray.get(i).getAsString());
                }
                if (users.size() == 0)
                    return;
                adapter = new FriendsAdapter(this, R.layout.contact, users);
                contactList.setAdapter(adapter);
                contactList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String contactName = users.get(position);
                        Intent intent = new Intent(ContactActivity.this, MessageActivity.class);
                        intent.putExtra("friendname", contactName);
                        intent.putExtra("Authorization", getIntent().getExtras().getString("Authorization"));
                        intent.putExtra("account", getIntent().getExtras().getString("account"));
                        intent.putExtra("password", getIntent().getExtras().getString("password"));
                        startActivity(intent);
                    }
                });
            } else {
                Toast.makeText(this, json.get("error").getAsString(), Toast.LENGTH_SHORT).show();
            }
        }
        else if (json.get("request").getAsString().equals("add friend")) {
            if (json.get("status").getAsString().equals("OK")) {
                launchGetFriendsRequest();
            }
            else {
                Toast.makeText(this, json.get("error").getAsString(), Toast.LENGTH_SHORT).show();
            }
        }
        else if (json.get("request").getAsString().equals("remove friend")) {
            if (json.get("status").getAsString().equals("OK")) {
                Toast.makeText(this, "Correctly removed", Toast.LENGTH_SHORT).show();
                launchGetFriendsRequest();
                contactName.setText("");
            }
            else
                Toast.makeText(this, json.get("error").getAsString(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
