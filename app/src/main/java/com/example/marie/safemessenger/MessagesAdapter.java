package com.example.marie.safemessenger;

import android.content.Context;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

/**
 * Created by marie on 2/13/15.
 */
public class MessagesAdapter extends ArrayAdapter<Message> {
    List<Message> messages;
    int layoutId;
    String currentUser;

    MessagesAdapter(Context context, int resourceId, List<Message> messages, String currentUser) {
        super(context, resourceId, messages);
        this.messages = messages;
        layoutId = resourceId;
        this.currentUser = currentUser;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View        retView = convertView == null ? View.inflate(getContext(), R.layout.message, null) : convertView;
        TextView    sender, content, date, msgId;
        ImageButton deleteBtn;
        String      dateString;
        Calendar    cal = Calendar.getInstance();
        Integer     day, month, year, hour, minutes;
        Typeface    tf = Typeface.createFromAsset(getContext().getAssets(), "timeburner.ttf");

        sender = (TextView) retView.findViewById(R.id.sender);
        sender.setTypeface(tf);
        content = (TextView) retView.findViewById(R.id.content);
        content.setTypeface(tf);
        date = (TextView) retView.findViewById(R.id.date);
        date.setTypeface(tf);
        msgId = (TextView) retView.findViewById(R.id.id_label);
        deleteBtn = (ImageButton) retView.findViewById(R.id.delete_btn);
        deleteBtn.setVisibility(View.VISIBLE);

        sender.setText(messages.get(position).getSender());
        content.setText(messages.get(position).getContent());
        cal.setTimeInMillis(messages.get(position).getTimestamp());
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH) + 1;
        year = cal.get(Calendar.YEAR);
        hour = cal.get(Calendar.HOUR_OF_DAY);
        minutes = cal.get(Calendar.MINUTE);
        dateString = day.toString() + "/" + month.toString() + "/" + year.toString() + " " + hour.toString() + ":" + minutes.toString();
        date.setText(dateString);
        msgId.setText(messages.get(position).get_id());
        if (!sender.getText().toString().equals(currentUser))
            deleteBtn.setVisibility(View.GONE);
        return retView;
    }
}
