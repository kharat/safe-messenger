package com.example.marie.safemessenger;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.client.methods.HttpGet;
import org.w3c.dom.Text;


public class LauncherActivity extends Activity implements View.OnClickListener, AsyncResponse {
    EditText account, password;
    Button connect, signUp;

    private void setUpAttributes() {
        TextView    title = (TextView) findViewById(R.id.title);
        Typeface face = Typeface.createFromAsset(getAssets(),
                "goodtimes.ttf");
        title.setTypeface(face);
        account = (EditText) findViewById(R.id.accountedit);
        password = (EditText) findViewById(R.id.pwdedit);
        connect = (Button) findViewById(R.id.connectbutton);
        signUp = (Button) findViewById(R.id.signupbutton);
        connect.setOnClickListener(this);
        signUp.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        setUpAttributes();
    }

    private void        launchSigninRequest() {
        HttpGet     getRequest = new HttpGet("http://vps120211.ovh.net:8080/safemessenger/rest/users/signin");
        String      token = account.getEditableText().toString() + ":" + password.getEditableText().toString();

        getRequest.addHeader("Authorization", "Basic " + Base64.encodeToString(token.getBytes(), Base64.NO_WRAP));
        new RequestMaker(this, this, getRequest).execute();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.connectbutton) {
           if (account.getText().length() > 0 && password.getText().length() > 0)
               launchSigninRequest();
        }
        else if (v.getId() == R.id.signupbutton) {
            Intent intent = new Intent(this, SignUp.class);
            startActivity(intent);
        }
    }

    @Override
    public void getResponse(String response) {
        JsonElement     parsed = new JsonParser().parse(response);
        JsonObject      json = parsed.getAsJsonObject();

        if (json.get("status").toString().contains("OK")) {
            Intent      intent = new Intent(this, ContactActivity.class);

            intent.putExtra("Authorization", json.get("token").getAsString());
            intent.putExtra("account", account.getEditableText().toString());
            intent.putExtra("password", password.getText().toString());
            startActivity(intent);
        }
        else {
            Toast.makeText(this, json.get("error").getAsString(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_launcher, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
